from copy import copy
import random

SPLITTER = "-"

def get_key():
    key = list("abcdefghijklmnopqrstuvwxyz")
    random.shuffle(key)
    return key

def get_cypher(word_list, document):
    key = get_key()
    encoded_lines = encode_lines(document, key)
    encoded_words = [encode(word, key) for word in word_list]
    assert verify(encoded_words, encoded_lines), "Cypher not sufficient for document."
    return (encoded_words, encoded_lines)

def encode_lines(document, key):
    result = list()
    for line, targets in document:
        replace_line = line.lower()
        for target in targets:
            replace_line = replace_line.replace(target, encode(target, key))
        result.append(replace_line)
    return result

def encode(the_text, key):
    result = list()
    for character in the_text:
        lookup_char = character.lower()
        new_char = key.index(lookup_char) if lookup_char in key else character
        result.append(str(new_char))
    return SPLITTER.join(result)

def verify(encoded_words, encoded_lines):
    all_encoded_lines = "".join(encoded_lines)
    all_encoded_words = SPLITTER.join(encoded_words)
    for char in all_encoded_words:
        if char != SPLITTER and char not in all_encoded_lines:
            return False
    return True

def print_cypher(cypher):
    words, lines = cypher
    print("The lines are:")
    for line in lines:
        print(line)
    print("Your encoded words are: {}".format(", ".join(words)))

def encode(the_text, key):
    result = list()
    for character in the_text:
        lookup_char = character.lower()
        new_char = key.index(lookup_char) if lookup_char in key else character
        result.append(str(new_char))
    return SPLITTER.join(result)

def verify(encoded_words, encoded_lines):
    all_encoded_lines = "".join(encoded_lines)
    all_encoded_words = SPLITTER.join(encoded_words)
    for char in all_encoded_words:
        if char != SPLITTER and char not in all_encoded_lines:
            return False
    return True



def run():

    data = [
        (["a", "cinepolis"], [
            ("I'm gonna make him an offer he can't refuse.", ["refuse"]),
            ("Toto, I've a feeling we're not in Kansas anymore.", ["anymore"]),
            ("May the Force be with you.", ["Force"]),
            ("You talking to me?", ["talking"]),
            ("I want the truth! You can't handle the truth!", ["handle"]),
            ("My mama always said life was like a box of chocolates. You never know what you're gonna get.", ["life"]),
            ("Houston, we have a problem.", ["problem"]),
        ]),
        (["vamos"],[
            ("Turdus grayi", ["turdus", "grayi"]),
            ("Ara macao", ["ara", "macao"]),
            ("Piranga rubra", ["piranga", "rubra"]),
            ("Butorides virescens", ["butorides", "virescens"]),
        ]),
        (["todos"],[
            ("Therefore the Lord himself will give you a sign: The virgin will conceive and give birth to a son and will call him Immanuel.", ["son"]),
            ("For God so loved the world that he gave his one and only Son, that whoever believes in him shall not perish but have eternal life.", ["god"]),
            ("After Jesus was born in Bethlehem in Judea, during the time of King Herod, Magi from the east came to Jerusalem", ["bethlehem"])
        ]),

    ]

    for words, lines in data:
        cypher = get_cypher(words, lines)
        print_cypher(cypher)

if __name__ == "__main__":
    run()
